import React from 'react'
import LoginScreen from './src/screen/LoginScreen'
import MainScreen from './src/screen/MainScreen'
import RegisterScreen from './src/screen/RegisterScreen'
import CheckinScreen from './src/screen/CheckinScreen'
import TimelineScreen from './src/screen/TimelineScreen'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

export default function App() {
  const Stack = createNativeStackNavigator();
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="LoginScreen" screenOptions={{headerShown: false}}>
        <Stack.Screen name="LoginScreen" component={LoginScreen} />
        <Stack.Screen name="MainScreen" component={MainScreen} />
        <Stack.Screen name="RegisterScreen" component={RegisterScreen} />
        <Stack.Screen name="CheckinScreen" component={CheckinScreen} />
        <Stack.Screen name="TimelineScreen" component={TimelineScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
}
