export const getCovidToday = async () => {
    try {
    const response = await fetch('https://covid19.ddc.moph.go.th/api/Cases/today-cases-all');
     const json = await response.json();
     return json;
   } catch (error) {
     console.error(error);
   }
 }

 export const getCovidAllDay = async () => {
    try {
     const response = await fetch('	https://covid19.ddc.moph.go.th/api/Cases/timeline-cases-all');
     const json = await response.json();
     return json;
   } catch (error) {
     console.error(error);
   }
 }