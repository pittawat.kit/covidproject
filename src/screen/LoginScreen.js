import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet } from 'react-native';
import moment from 'moment';
export default function LoginScreen({ navigation }) {
  const [isLoading, setisLoading] = useState(false);
  const [data, setdata] = useState({});

  useEffect(() => {
    setisLoading(true);
    getCovidToday();
    setisLoading(false);
  }, []);

  const CheckIn = () => {
    navigation.navigate('CheckinScreen');
  };
  const Timeline = () => {
    navigation.navigate('TimelineScreen');
  };

  const getCovidToday = async () => {
    const response = await fetch(
      'https://covid19.ddc.moph.go.th/api/Cases/today-cases-all',
    );
    const json = await response.json();
    console.log(json[0]);
    setdata(json[0]);
  };
  const image = { uri: "https://images-ext-2.discordapp.net/external/qqvcoX5XuOaFkbjRsdsVp99UZROYcATfZoZNxjhQPZ4/https/image.freepik.com/free-vector/flat-hand-drawn-doctor-injecting-vaccine-patient_23-2148869091.jpg" };
  return (
    <View>
      {isLoading == true ? null : (
        <View style={styles.container}>
          <ImageBackground source={image} style={styles.image}>
            <View style={{backgroundColor:'rgba(52, 52, 52, 0.55)'}}>
              <Text style={styles.text}>รายงานผู้ติดเชื้อวันที่ {moment(data.txn_date).format("DD MMMM YYYY")} </Text>
              <Text style={styles.text}>จำนวนผู้ป่วยรายใหม่ : {data.new_case}</Text>
              <Text style={styles.text}>จำนวนผู้ป่วยสะสม : {data.total_case}</Text>
              <Text style={styles.text}>
                จำนวนผู้ป่วยรายใหม่ :{' '}
                {data.new_case_excludeabroad}
              </Text>
              <Text style={styles.text}>
                จำนวนผู้ป่วยสะสม :{' '}
                {data.total_case_excludeabroad}
              </Text >
              <Text style={styles.text}>จำนวนผู้ป่วยตายสะสม : {data.new_death}</Text>
              <Text style={styles.text}>จำนวนผู้ป่วยรักษาหายรายใหม่ : {data.new_recovered}</Text>
              <Text style={styles.text}>จำนวนผู้ป่วยรักษาหายสะสม : {data.total_recovered}</Text>
              <TouchableOpacity style={{ borderWidth: 0.2 }} onPress={() => CheckIn()}>
                <Text style={[styles.text, { backgroundColor:'rgba(52, 52, 52, 0.5)' }]}>Check In</Text>
              </TouchableOpacity>
              <TouchableOpacity style={{ borderWidth: 0.2, }} onPress={() => Timeline()}>
                <Text style={[styles.text, { backgroundColor:'rgba(52, 52, 52, 0.5)' }]}>Time line</Text>
              </TouchableOpacity>
            </View>
          </ImageBackground>
        </View>
      )}

    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    // flex: 1,

  },
  image: {
    // flex: 1,
    // justifyContent: "center"
  },
  text: {
    color: "#ffff",
    fontSize: 18,
    lineHeight: 76,
    fontWeight: "bold",
    textAlign: "center",
    // backgroundColor: "#CCFFCCbb",
  }
});