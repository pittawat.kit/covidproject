import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, TextInput,ImageBackground, StyleSheet} from 'react-native';
import GetLocation from 'react-native-get-location';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function CheckinScreen({ navigation }) {
  const [isLoading, setisLoading] = useState(false);
  const [latitude, setlatitude] = useState('');
  const [longitude, setlongitude] = useState('');
  const [locationName, setlocationName] = useState('');
  const [memo, setmemo] = useState('');

  useEffect(() => {
    setisLoading(true);
    Getlocation();
    setisLoading(false);
  }, []);

  const Getlocation = () => {
    GetLocation.getCurrentPosition({
      enableHighAccuracy: true,
      timeout: 15000,
    })
      .then(location => {
        setlatitude(location.latitude);
        setlongitude(location.longitude);
      })
      .catch(error => {
        const {code, message} = error;
        console.warn(code, message);
      });
  };

  const inputLocationNameChange = val => {
    try {
      setlocationName(val);
    } catch (error) {
      Alert.alert('Error', error);
    }
  };

  const inputMemoChange = val => {
    try {
      setmemo(val);
    } catch (error) {
      Alert.alert('Error', error);
    }
  };

  const CheckIn = async () => {
    try {
        var timeline = await AsyncStorage.getItem('Timeline');
    console.log("prev Timeline : " + timeline);
    var mockarr = [];
    if (timeline != null) {
        mockarr = JSON.parse(timeline);
        console.log("prev Timeline in arr : " + timeline.toString());
    }

    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth())+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;

    var mockdata = {
      lat: latitude,
      long: longitude,
      Name: locationName,
      memo: memo,
      dateTime: dateTime
    };

    console.log("mockdata : " + JSON.stringify(mockdata));
    mockarr.push(mockdata);
    await AsyncStorage.setItem('Timeline',JSON.stringify(mockarr));
    navigation.reset({
        index: 0,
        routes: [{ name: 'LoginScreen' }],
      });

    } catch (error) {
        Alert.alert('Error CheckIn', error);
    }
  };
  const image = { uri: "https://images-ext-1.discordapp.net/external/DCA9fM72PPJXGbR6nEl8OzHioz2YA1zmYE5SG-MO85w/https/image.freepik.com/free-vector/reopening-business-background-with-design-space_53876-114230.jpg" };
  return (
    <View>
      {isLoading == true ? null : (
        <View style={styles.container}>
           <ImageBackground source={require('../upload/covid01.jpg')} style={styles.image}>
          <Text style={[styles.text,{textAlign: "center",color: "black"}]} >Check In Location</Text>
          <Text style={styles.text} >latitude : {latitude}</Text>
          <Text style={styles.text}>longitude : {longitude}</Text>
          <TextInput style={[styles.text,{borderWidth:1,alignSelf: 'center',width:'70%',marginTop:30}]}
            value={locationName}
            onChangeText={val => inputLocationNameChange(val)}
            placeholder={'ชื่อสถานที่'}
          />
          <TextInput style={[styles.text,{borderWidth:1,alignSelf: 'center',width:'70%',marginTop:30}]}
            value={memo}
            onChangeText={val => inputMemoChange(val)}
            placeholder={'หมายเหตุ'}
          />
          <View style={{width: '50%', alignItems: 'center',
           justifyContent: 'center', alignSelf: 'center', 
           backgroundColor:'rgba(780, 0, 55, 0.8)',height:'7%',marginTop:30}}>
          <TouchableOpacity onPress={() => CheckIn()}>
            <Text style={[styles.text,{lineHeight:50,color:'white'}]}>Check In</Text>
          </TouchableOpacity>
          </View>
          <View style={{ marginTop:160,width: '50%', alignItems: 'center',
           justifyContent: 'center',  
           backgroundColor:'rgba(1, 0, 130, 0.8)',height:'7%'}}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Text style={{fontSize:20,color:'white'}}>Back</Text>
          </TouchableOpacity>
          </View>
          </ImageBackground>
        </View>
      )}
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
    width: '100%',
    height: '100%',    
  },
  item: {
   backgroundColor:'rgba(100, 100, 100, 0.8)',
    padding: 30,
    marginVertical: 5,
    marginHorizontal: 16
  },
  title: {
    fontSize: 18,
    color:"pink"
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
  text: {
    color: "red",
    fontSize: 18,
    lineHeight: 72,
    fontWeight: "bold",
    // textAlign: "center",
    // backgroundColor: "#CCFFCCbb",
  }
});