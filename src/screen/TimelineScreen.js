import React, { useEffect, useState } from 'react';
import { View, Text, TouchableOpacity, FlatList, StyleSheet, StatusBar, ImageBackground } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default function TimelineScreen({ navigation }) {
  const [isLoading, setisLoading] = useState(false);
  const [timeline, settimeline] = useState([]);
  useEffect(() => {
    setisLoading(true);
    getData();
    setisLoading(false);
  }, []);

  const getData = async () => {
    var timeline = await AsyncStorage.getItem('Timeline');
    var mockarr = [];
    if (timeline != null) {
      mockarr = JSON.parse(timeline);
    }
    settimeline(mockarr);
    console.log(mockarr);
  };

  const Item = ({ title }) => (
    <View style={styles.item}>
      <Text style={styles.title}>สถานที่ : {title.Name}</Text>
      <Text style={styles.title}>lat,long : {title.lat} , {title.long}</Text>
      <Text style={styles.title}>วันเวลา : {title.dateTime}</Text>
      <Text style={styles.title}>หมายเหตุ : {title.memo}</Text>
    </View>
  );

  const renderItem = ({ item }) => (
    <Item title={item} />
  );

  return (
    <View >
      {isLoading == true ? null : (
        <View style={styles.container} >
            <ImageBackground source={require('../upload/covid02.jpg')} style={styles.image}>
          <Text style={{fontSize:24,padding:10,color:"white",alignSelf:'center'}}>Timeline</Text>
          <FlatList
            data={timeline}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
          <View style={{ width: '50%', alignItems: 'center', justifyContent: 'center',alignSelf: 'center',  backgroundColor:'rgba(1, 0, 130, 0.8)',height:'7%' }}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Text style={{fontSize:20,color:'white'}}>กลับสู่หน้าหลัก</Text>
            </TouchableOpacity>
          </View>
          </ImageBackground>
        </View>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
    width: '100%',
    height: '100%',
    
  },
  item: {
   backgroundColor:'rgba(100, 100, 100, 0.8)',
    padding: 30,
    marginVertical: 5,
    marginHorizontal: 16
  },
  title: {
    fontSize: 18,
    color:"pink"
  },
  image: {
    flex: 1,
    // justifyContent: "center"
  },
});